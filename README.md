# Google

- [My Activity](https://myactivity.google.com/myactivity)
- [Dashboard](https://myaccount.google.com/dashboard)
- [Privacy Checkup](https://myaccount.google.com/privacycheckup)
- [Ads Settings](https://adssettings.google.com/) for turning off personalized ads
